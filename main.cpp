// Name: Andre De Jager | Student Number: 38762447 | COS1511 Assignment 2 | 872356
// Question 1

#include <iostream>
using namespace std;

// A functions that prompts and returns the users weight and height using reference types
void getData(float & weightP, float & heightP)
{
    // Prompt user for weight with clear instructions and don't allow negatives or zero
    do{
        cout << "Step 1: Please enter your weight in kilograms (eg 85.5 ) and hit enter" << endl;
        cin >> weightP;

    } while (weightP <= 0);

    // Prompt user for height with clear instructions and don't allow negatives or zero
    do{
        cout << "Step 2: Now enter your height in meters (eg 1.76 ) and hit enter" << endl;
        cin >> heightP;

    } while (heightP <= 0);
}

// This function calculates the BMI for a given weight and height returning it as a float
float calcBMI(float weightP, float heightP)
{
    float bmi;

    bmi = weightP / (heightP * heightP);

    return bmi;
}

// Display the fitness result
void displayFitnessResults(float bmiP)
{
    string weightStatus;

    if(bmiP >= 30.0)
    {
        // 1. if bmi is equal to or greater than 30 kg then Obese
        weightStatus = "Obese";
    }
    else
    {
        if(bmiP < 18.5)
        {
            // 2. if bmi is less than 18.5 kg then Underweight
            weightStatus = "Underweight";
        }
        else if (bmiP >= 18.5 && bmiP <= 24.9)
        {
            // 3. if bmi is between 18.5 - 24.9  then Healthy
            weightStatus = "Healthy";
        }
        else
        {
            // 4. if no match then bmi is between 25.0 ‐ 29.9 and Overweight
            weightStatus = "Overweight";
        }
    }

    //5. Output BMI and Weight Status to user
    cout << "Result: With a BMI of " << bmiP << " kg/m2, your Weight Status is " << weightStatus << "." << endl;
}

// The main function
int main()
{
    // 1. Declare required variables
    float weight, height, bmi;

    // 2. Prompt and collect data from user passing in weight and height by ref
    getData(weight, height);

    // 3. Call the calculateBMI passing in weight and height from user
    bmi = calcBMI(weight, height);

    // 4. Display the users fitness result (BMI and Weight Status)
    displayFitnessResults(bmi);

return 0;
}
